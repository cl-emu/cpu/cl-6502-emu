;;;; cl-6502-emu.lisp

(in-package #:cl-6502-emu)

(deftype u8 () '(unsigned-byte 8))
(deftype u16 () '(unsigned-byte 16))

(defstruct cpu-6502
  (pc 0 :type u16)
  (a 0 :type u8)
  (x 0 :type u8)
  (y 0 :type u8)
  (s #xfd :type u8)
  (flags #x24 :type u8)
  (opcode 0 :type u8)
  (cycles 0 :type (integer 0))
  (decimal-mode-enabled t :type boolean))

(defgeneric 6502-read-byte (cpu address)
  (:documentation "Read a byte from ADDRESS for the CPU and return it."))

(defgeneric 6502-write-byte (cpu address data)
  (:documentation "Write a byte at ADDRESS for the CPU
using the value of DATA and then return DATA."))

(defun 6502-read-cycle (cpu address)
  (prog1
    (6502-read-byte cpu address)
    (6502-add-cycles cpu 1)))

(defun 6502-write-cycle (cpu address data)
  (6502-write-byte cpu address data)
  (6502-add-cycles cpu 1))

(defgeneric 6502-sync (cpu)
  (:documentation "Synchronize the other parts of the system with the CPU."))

(defun 6502-add-cycles (cpu value)
  (setf (cpu-6502-cycles cpu) (+ (cpu-6502-cycles cpu) value))
  (dotimes (i value)
    (6502-sync cpu)))

(defvar *opcode-functions* (make-array #x100 :initial-element (constantly nil)))

(defmacro definstruction (name &body opcode-specs)
  (let ((opcode-syms
          (mapcar
           (lambda (spec)
             (format-symbol (symbol-package name)
                            "~A-~{~A~^-~}"
                            name (car spec)))
           opcode-specs)))
    `(progn
       ,@(loop :for opcode-sym :in opcode-syms
               :for (addressing-modes opcodes . body) :in opcode-specs
               :nconc
               (list*
                `(defun ,opcode-sym (cpu)
                   (incf (cpu-6502-pc cpu))
                   (6502-add-cycles cpu 1)
                   ,@body)
                (mapcar
                 (lambda (opcode)
                   `(setf (svref *opcode-functions* ,opcode) #',opcode-sym))
                 opcodes))))))

(defmacro defopcode-read (name opcode-specs &body body)
  (let ((implied-specs (assoc :implied opcode-specs :key #'car))
        (immediate-specs (assoc :immediate opcode-specs :key #'car))
        (absolute-specs (assoc :absolute opcode-specs :key #'car))
        (zero-page-specs (assoc :zero-page opcode-specs :key #'car))
        (zero-page-x-specs (assoc :zero-page-x opcode-specs :key #'car))
        (zero-page-y-specs (assoc :zero-page-y opcode-specs :key #'car)))
    `(definstruction ,name
       ,@(when implied-specs
           (list
            `(,@implied-specs
              (6502-read-cycle cpu (cpu-6502-pc cpu))
              ,@body)))
       ,@(when immediate-specs
           (list
            `(,@immediate-specs
              (let ((value (6502-read-cycle cpu (cpu-6502-pc cpu))))
                (declare (ignorable value))
                ,@body)
              (incf (cpu-6502-pc cpu)))))
       ,@(when absolute-specs
           (list
            `(,@absolute-specs
              (let* ((low-byte (6502-read-cycle cpu (cpu-6502-pc cpu)))
                    (high-byte (6502-read-cycle cpu (1+ (cpu-6502-pc cpu))))
                    (effective-address (logior low-byte (ash high-byte 8)))
                    (value (6502-read-cycle cpu effective-address)))
                (declare (ignorable low-byte high-byte effective-address value))
                ,@body)
              (setf (cpu-6502-pc cpu) (+ (cpu-6502-pc cpu) 2)))))
       ,@(when zero-page-specs
           (list
            `(,@zero-page-specs
              (let* ((address (6502-read-cycle cpu (cpu-6502-pc cpu)))
                    (value (6502-read-cycle cpu address)))
                (declare (ignorable address value))
                ,@body
                (incf (cpu-6502-pc cpu))))))
       ,@(when zero-page-x-specs
           (list
            `(,@zero-page-x-specs
              (let ((bad-address (6502-read-cycle cpu (cpu-6502-pc cpu))))
                (6502-read-cycle cpu bad-address)
                (let* ((address (logand (+ bad-address (cpu-6502-x cpu)) #xFF))
                       (value (6502-read-cycle cpu address)))
                  (declare (ignorable address value))
                  ,@body
                  (incf (cpu-6502-pc cpu)))))))
       ,@(when zero-page-y-specs
           (list
            `(,@zero-page-y-specs
              (let ((bad-address (6502-read-cycle cpu (cpu-6502-pc cpu))))
                (6502-read-cycle cpu bad-address)
                (let* ((address (logand (+ bad-address (cpu-6502-y cpu)) #xFF))
                       (value (6502-read-cycle cpu address)))
                  (declare (ignorable address value))
                  ,@body
                  (incf (cpu-6502-pc cpu))))))))))

(defmacro defopcode-write (name opcode-specs &body body)
  (let ((absolute-specs (assoc :absolute opcode-specs :key #'car))
        (zero-page-specs (assoc :zero-page opcode-specs :key #'car)))
    `(definstruction ,name
       ,@(when absolute-specs
           (list
            `(,@absolute-specs
              (let* ((low-byte (6502-read-cycle cpu (cpu-6502-pc cpu)))
                     (high-byte (6502-read-cycle cpu (1+ (cpu-6502-pc cpu))))
                     (address (logior low-byte (ash high-byte 8))))
                (declare (ignorable low-byte high-byte address))
                ,@body)
              (setf (cpu-6502-pc cpu) (+ (cpu-6502-pc cpu) 2)))))
       ,@(when zero-page-specs
           (list
            `(,@zero-page-specs
              (let ((address (6502-read-cycle cpu (cpu-6502-pc cpu))))
                (declare (ignorable address))
                ,@body)
              (incf (cpu-6502-pc cpu))))))))

(defopcode-read nop
    (((:implied) (#x1A #x3A #x5A #x7A #xDA #xEA #xFA))
     ((:immediate) (#x80 #x82 #xC2 #x89 #xE2))
     ((:absolute) (#x0C))
     ((:zero-page) (#x04 #x44 #x64))
     ((:zero-page-x) (#x14 #x34 #x54 #x74 #xD4 #xF4)))
  (values))

(defopcode-read clc (((:implied) (#x18)))
 (setf (cpu-6502-flags cpu) (logand (cpu-6502-flags cpu) (lognot #x01))))

(defopcode-read sec (((:implied) (#x38)))
 (setf (cpu-6502-flags cpu) (logior (cpu-6502-flags cpu) #x01)))

(defopcode-read cli (((:implied) (#x58)))
 (setf (cpu-6502-flags cpu) (logand (cpu-6502-flags cpu) (lognot #x04))))

(defopcode-read sei (((:implied) (#x78)))
 (setf (cpu-6502-flags cpu) (logior (cpu-6502-flags cpu) #x04)))

(defopcode-read clv (((:implied) (#xB8)))
 (setf (cpu-6502-flags cpu) (logand (cpu-6502-flags cpu) (lognot #x40))))

(defopcode-read cld (((:implied) (#xD8)))
 (setf (cpu-6502-flags cpu) (logand (cpu-6502-flags cpu) (lognot #x08))))

(defopcode-read sed (((:implied) (#xF8)))
 (setf (cpu-6502-flags cpu) (logior (cpu-6502-flags cpu) #x08)))

(defun set-zero-flag (cpu value)
  (if (= value 0)
      (setf (cpu-6502-flags cpu) (logior (cpu-6502-flags cpu) #x02))
      (setf (cpu-6502-flags cpu) (logand (cpu-6502-flags cpu) (lognot #x02)))))

(defun set-sign-flag (cpu value)
  (if (= (logand value #x80) #x80)
      (setf (cpu-6502-flags cpu) (logior (cpu-6502-flags cpu) #x80))
      (setf (cpu-6502-flags cpu) (logand (cpu-6502-flags cpu) (lognot #x80)))))

(defun set-carry-flag (cpu value)
  (if (<= value #x100)
      (setf (cpu-6502-flags cpu) (logior (cpu-6502-flags cpu) #x01))
      (setf (cpu-6502-flags cpu) (logand (cpu-6502-flags cpu) (lognot #x01)))))

(defopcode-read lda (((:immediate) (#xA9))
                     ((:absolute) (#xAD))
                     ((:zero-page) (#xA5))
                     ((:zero-page-x) (#xB5)))
  (setf (cpu-6502-a cpu) value)
  (set-zero-flag cpu value)
  (set-sign-flag cpu value))

(defopcode-read ldx (((:immediate) (#xA2))
                     ((:absolute) (#xAE))
                     ((:zero-page) (#xA6))
                     ((:zero-page-y) (#xB6)))
  (setf (cpu-6502-x cpu) value)
  (set-zero-flag cpu value)
  (set-sign-flag cpu value))

(defopcode-read ldy (((:immediate) (#xA0))
                     ((:absolute) (#xAC))
                     ((:zero-page) (#xA4))
                     ((:zero-page-x) (#xB4)))
  (setf (cpu-6502-y cpu) value)
  (set-zero-flag cpu value)
  (set-sign-flag cpu value))

(defopcode-write sta (((:absolute) (#x8D))
                      ((:zero-page) (#x85)))
  (6502-write-cycle cpu address (cpu-6502-a cpu)))

(defopcode-write stx (((:absolute) (#x8E))
                      ((:zero-page) (#x86)))
  (6502-write-cycle cpu address (cpu-6502-x cpu)))

(defopcode-write sty (((:absolute) (#x8C))
                      ((:zero-page) (#x84)))
  (6502-write-cycle cpu address (cpu-6502-y cpu)))

(defopcode-read tax (((:implied) (#xAA)))
  (setf (cpu-6502-x cpu) (cpu-6502-a cpu))
  (set-zero-flag cpu (cpu-6502-x cpu))
  (set-sign-flag cpu (cpu-6502-x cpu)))

(defopcode-read txa (((:implied) (#x8A)))
  (setf (cpu-6502-a cpu) (cpu-6502-x cpu))
  (set-zero-flag cpu (cpu-6502-a cpu))
  (set-sign-flag cpu (cpu-6502-a cpu)))

(defopcode-read tsx (((:implied) (#xBA)))
 (setf (cpu-6502-x cpu) (cpu-6502-s cpu)))

(defopcode-read txs (((:implied) (#x9A)))
 (setf (cpu-6502-s cpu) (cpu-6502-x cpu)))

(defopcode-read tay (((:implied) (#xA8)))
  (setf (cpu-6502-y cpu) (cpu-6502-a cpu))
  (set-zero-flag cpu (cpu-6502-y cpu))
  (set-sign-flag cpu (cpu-6502-y cpu)))

(defopcode-read tya (((:implied) (#x98)))
  (setf (cpu-6502-a cpu) (cpu-6502-y cpu))
  (set-zero-flag cpu (cpu-6502-a cpu))
  (set-sign-flag cpu (cpu-6502-a cpu)))

(defopcode-read dex (((:implied) (#xCA)))
  (decf (cpu-6502-x cpu))
  (set-zero-flag cpu (cpu-6502-x cpu))
  (set-sign-flag cpu (cpu-6502-x cpu)))

(defopcode-read inx (((:implied) (#xE8)))
  (incf (cpu-6502-x cpu))
  (set-zero-flag cpu (cpu-6502-x cpu))
  (set-sign-flag cpu (cpu-6502-x cpu)))

(defopcode-read dey (((:implied) (#x88)))
  (decf (cpu-6502-y cpu))
  (set-zero-flag cpu (cpu-6502-y cpu))
  (set-sign-flag cpu (cpu-6502-y cpu)))

(defopcode-read iny (((:implied) (#xC8)))
  (incf (cpu-6502-y cpu))
  (set-zero-flag cpu (cpu-6502-y cpu))
  (set-sign-flag cpu (cpu-6502-y cpu)))

(defopcode-read anda (((:immediate) (#x29))
                      ((:absolute) (#x2D))
                      ((:zero-page) (#x25))
                      ((:zero-page-x) (#x35)))
  (setf (cpu-6502-a cpu) (logand (cpu-6502-a cpu) value))
  (set-zero-flag cpu value)
  (set-sign-flag cpu value))

(defopcode-read ora (((:immediate) (#x09))
                     ((:absolute) (#x0D))
                     ((:zero-page) (#x05))
                     ((:zero-page-x) (#x15)))
  (setf (cpu-6502-a cpu) (logior (cpu-6502-a cpu) value))
  (set-zero-flag cpu value)
  (set-sign-flag cpu value))

(defopcode-read eor (((:immediate) (#x49))
                     ((:absolute) (#x4D))
                     ((:zero-page) (#x45))
                     ((:zero-page-x) (#x55)))
  (setf (cpu-6502-a cpu) (logxor (cpu-6502-a cpu) value))
  (set-zero-flag cpu value)
  (set-sign-flag cpu value))

(defopcode-read bit-6502 (((:absolute) (#x2C))
                     ((:zero-page) (#x24)))
  (let* ((temp (logand (cpu-6502-a cpu) value)))
    (set-zero-flag cpu temp)
    (set-sign-flag cpu temp)
    (if (= (logand temp #x40) #x40)
        (setf (cpu-6502-flags cpu) (logior (cpu-6502-flags cpu) #x40))
        (setf (cpu-6502-flags cpu) (logand (cpu-6502-flags cpu) (lognot #x40))))))

(defun kil-error (&rest args)
  (declare (ignore args))
  (error "KIL opcode encountered!"))

(dolist (opcode '(#x02 #x12 #x22 #x32 #x42 #x52 #x62 #x72 #x92 #xB2 #xD2 #xF2))
  (setf (svref *opcode-functions* opcode) #'kil-error))

(defun 6502-reset (cpu)
  (let ((reset-vector (logior (6502-read-byte cpu #xFFFC) (ash (6502-read-byte cpu #xFFFD) 8))))
    (setf (cpu-6502-pc cpu) reset-vector)))

(defun 6502-tick (cpu)
  (setf (cpu-6502-opcode cpu) (6502-read-byte cpu (cpu-6502-pc cpu)))
  (funcall (the function (svref *opcode-functions* (cpu-6502-opcode cpu))) cpu)
  (format t "Opcode ~X~%" (cpu-6502-opcode cpu)))
