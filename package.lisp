;;;; package.lisp

(defpackage #:cl-6502-emu
  (:use #:cl #:alexandria)
  (:export #:6502-read-byte #:6502-write-byte #:6502-sync
           #:6502-reset #:6502-tick #:make-cpu-6502))
